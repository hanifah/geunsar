<?php  
class Crud extends CI_Controller{
 
	function __construct(){
		parent::__construct();		
		$this->load->model('m_data');

	}
 
	function index(){
		$data['admin'] = $this->m_data->tampil_data()->result();
		$this->load->view('v_tampil',$data);


	}
 
	function tambah(){
		$this->load->view('v_input');
	}
	
	function tambah_aksi(){
		$kd_admin = $this->input->post('kd_admin');
		$nama_admin = $this->input->post('nama_admin');
		$alamat = $this->input->post('alamat');
		$notelp = $this->input->post('notelp');
		$email = $this->input->post('email');
 
		$data = array(
			'kd_admin' => $kd_admin,
			'nama_admin' => $nama_admin,
			'alamat' => $alamat,
			'notelp' => $notelp,
			'email' => $email
			);
		$this->m_data->input_data($data,'admin');
		redirect('crud/index');
	}
	
	function edit($kd_admin){
		$where = array('kd_admin' => $kd_admin);
		$data['admin'] = $this->m_data->edit_data($where,'admin')->result();
		$this->load->view('v_edit',$data);
	}
	
	function update(){
		$kd_admin = $this->input->post('kd_admin');
		$nama_admin = $this->input->post('nama_admin');
		$alamat = $this->input->post('alamat');
		$notelp = $this->input->post('notelp');
		$email = $this->input->post('email');
	 
		$data = array(
			'nama_admin' => $nama_admin,
			'alamat' => $alamat,
			'notelp' => $notelp,
			'email' => $email
		);
	 
		$where = array(
			'kd_admin' => $kd_admin
		);
	 
		$this->m_data->update_data($where,$data,'admin');
		redirect('crud/index');
	}
	
	function hapus($kd_admin){
		$where = array('kd_admin' => $kd_admin);
		$this->m_data->hapus_data($where,'admin');
		redirect('crud/index');
	}
 
}