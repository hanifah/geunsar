<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Jurusan extends CI_Controller {

	public function index()
	{
		$this->modelsqurity->getsqurity();
		$isi['content'] ='jurusan/tampiljurusan';
		$isi['judul'] = 'master';
		$isi['sub_judul'] = 'jurusan';
		$isi['data'] = $this->db->get('admin');
}
	public function tambah()
	{
		$this->modelsqurity->getsqurity();
		$isi['content'] ='jurusan/tambahjurusan';
		$isi['judul'] = 'master';
		$isi['sub_judul'] = 'tambah jurusan';
		$isi['kodeprodi'] ="";
		$isi['namaprodi'] = "";
		$isi['singkat'] ="";
		$isi['ketuaprodi'] = "";
		$isi['nik'] ="";
		$isi['akreditasi'] = "";
	}

		public function edit()
	{
		$this->modelsqurity->getsqurity();
		$isi['content'] ='jurusan/tambahjurusan';
		$isi['judul'] = 'master';
		$isi['sub_judul'] = 'edit jurusan';
		$key = $this->uri->segment(3);
		$this->db->where('admin',$key);
		$query = $this->db->get('admin');
		if ($query->num_rows()>0)
		 {
			foreach ($query->result() as $row) 
			{
				$isi['kodeprodi'] = $row->kodeprodi;
				$isi['namaprodi'] = $row->namaprodi;
				$isi['singkat'] = $row->singkat;
				$isi['ketuaprodi'] = $row->ketuaprodi;
				$isi['nik'] = $row->nik;
				$isi['akreditasi'] = $row->akreditasi;


			}
		}else
		{
			$isi['kodeprodi'] ="";
				$isi['namaprodi'] = "";
				$isi['singkat'] ="";
				$isi['ketuaprodi'] = "";
				$isi['nik'] ="";
				$isi['akreditasi'] = "";
		}
		

		$this->load->view('tampilanhome',$isi);
	}

	public function simpan()
	{
		$this->modelsqurity->getsqurity();

		 $key = $this->input->post('kodeprodi');
		 $data['kodeprodi'] = $this->input->post('kodeprodi');
		 $data['namaprodi'] = $this->input->post('namaprodi');
		  $data['singkat'] = $this->input->post('singkat');
		   $data['ketuaprodi'] = $this->input->post('ketuaprodi');
			$data['nik'] = $this->input->post('nik');
			$data['akreditasi'] = $this->input->post('akreditasi');

			$this->load->model('modeljurusan');
			$query =$this->modeljurusan->getdata($key);
			if($query->num_rows()>0)
			{
				$this->modeljurusan->getupdate($key,$data);
			}
			else
			{
				$this->modeljurusan->getinsert($data);
			}
			redirect('jurusan');




	
}
public function delete()
	{
		$this->modelsqurity->getsqurity();
		$this->load->model('modeljurusan');
		$key = $this->uri->segment(3);
		$this->db->where('kodeprodi',$key);
		$query = $this->db->get('t_prodi');
		if($query->num_rows()>0)
		{
			$this->modeljurusan->getdelete($key);
		}
		redirect('jurusan');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */