<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Modeljurusan extends CI_model {

	public function getdata($key)
	{
		$this->db->where('kd_admin',$key);
		$hasil = $this->db->get('admin');
		return $hasil;
	}

	public function getupdate($key,$data)
	{
		$this->db->where('kd_admin',$key);
		$this->db->update('admin',$data);
	}
	public function getinsert($data)
	{
		$this->db->insert('admin',$data);
	}
	public function getdelete($key)
	{
		$this->db->where('kd_admin',$key);
		$this->db->delete('admin');
	}

}
