<script type="text/javascript">
			jQuery(function($) {
				//initiate dataTables plugin
				var myTable = 
				$('#dynamic-table')
				//.wrap("<div class='dataTables_borderWrap' />")   //if you are applying horizontal scrolling (sScrollX)
				.DataTable( {
					"aoColumns": [
					  null, null,null, null, null,null,null,
					  { "bSortable": false }
					]});
			})
			</script>
					
				
<a href="<?php echo base_url();?>index.php/jurusan/tambah" class="btn btn-primary btn-small">tambah data</a>
<p>


<table id="dynamic-table" class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<th>no</th>
			<th>kode</th>
			<th>nama prodi</th>
			<th>singkatan</th>
			<th>ketua prodi</th>
			<th>NIK</th>
			<th>akreditasi</th>
			<th>aksi</th>

		</tr>
	</head>
	<tbody>
		<tr>
			<?php
			$no = 1;
			foreach ($data->result() as $row) {
				?>
				<td><?php echo $no++;?></td>
				<td><?php echo $row->kodeprodi;?> </td>
				<td><?php echo $row->namaprodi;?> </td>
				<td><?php echo $row->singkat;?> </td>
				<td><?php echo $row->ketuaprodi;?> </td>
				<td><?php echo $row->nik;?> </td>
				<td><?php echo $row->akreditasi;?> </td>
				<td>
					<a href="<?php echo base_url()?>index.php/jurusan/edit/<?php echo $row->kodeprodi; ?>">edit</a>
					<a href="<?php echo base_url()?>index.php/jurusan/delete/<?php echo $row->kodeprodi; ?>" onclick="return confirm('anda yakin mau menghapus data ini');">delete</a>
				</td>
				</tr>
				<?php } ?>
			</tbody>
</table>