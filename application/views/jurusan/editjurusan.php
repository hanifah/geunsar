<script type="text/javascript">
function cekform()
{
	if(!$("#kodeprodi").val())
	{
		alert('kode tidak boleh kosong');
		$("#kodeprodi").focus()
			return false;
	}
	if(!$("#namaprodi").val())
	{
		alert('nama prodi tidak boleh kosong');
		$("#namaprodi").focus()
			return false;
	}
	if(!$("#singkat").val())
	{
		alert('singkatan tidak boleh kosong');
		$("#singkat").focus()
			return false;
	}
	if(!$("#ketuaprodi").val())
	{
		alert('ketua prodi tidak boleh kosong');
		$("#ketuaprodi").focus()
			return false;
	}
	if(!$("#nik").val())
	{
		alert('nik tidak boleh kosong');
		$("#nik").focus()
			return false;
	}
	if(!$("#akreditasi").val())
	{
		alert('akreditasi tidak boleh kosong');
		$("#akreditasi").focus()
			return false;
	}
}
</script>


<form class="form-horizontal" method="POST" action="<?php echo base_url();?>index.php/jurusan/simpan" onsubmit="return cekform();">
	<div class="control-group">
		<label class="control-label">kode jurusan</label>
		<div class="controls">
			<input type="text" name="kodeprodi" id="kodeprodi" placeholder="kode" class="span1" value="<?php echo $kodeprodi; ?>">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">jurusan</label>
		<div class="controls">
			<input type="text" name="namaprodi" id="namaprodi" placeholder="prodi" class="span3"value="<?php echo $namaprodi; ?>">
		</div>
	</div>
	<div class="control-group">
		<label class="control-label">singkatan</label>
		<div class="controls">
			<input type="text" name="singkat" id="singkat" placeholder="singkatan" class="span1"value="<?php echo $singkat; ?>">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">ketua prodi</label>
		<div class="controls">
			<input type="text" name="ketuaprodi" id="ketuaprodi" placeholder="ketua prodi" class="span1"value="<?php echo $ketuaprodi; ?>">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">nik</label>
		<div class="controls">
			<input type="text" name="nik" id="nik" placeholder="nik" class="span1"value="<?php echo $nik; ?>">
		</div>
	</div>

	<div class="control-group">
		<label class="control-label">akreditasi</label>
		<div class="controls">
			<input type="text" name="akreditasi" id="akreditasi" placeholder="akreditasi" class="span1"value="<?php echo $akreditasi; ?>">
		</div>
	</div>
	<div>
		&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
		<button type="submit" class="btn btn-primary btn-small">simpan</button>
		<a href ="<?php echo base_url();?>index.php/jurusan" class="btn btn-primary btn-small">tutup</a>
	</div>

</form>