<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 

class Migration_pembayaran extends CI_Migration
{
    
 
    public function up()
    {
    	if (!$this->db->table_exists('pembayaran')) {
    		$this->dbforge->add_field( array(
    			'no_faktur' => array(
					'type'           => 'int',
					'constraint'     => 10,
					'unsigned'       => TRUE,
					'auto_increment' => TRUE
    			),
    			'no_ktp' => array(
					'type'       => 'varchar',
					'constraint' => 16,
					'null'       => false
    			),
				'bukti_pembayaran' => array(
					'type'       => 'varchar',
					'constraint' => 16,
					'null'       => false
    			),
    		));
 
    		
    		$this->dbforge->add_key('no_faktur', true);
    		$this->dbforge->create_table('pembayaran');
 
    	} 
    }
 
   
    
    public function down()
    {
    	$this->dbforge->drop_table('pembayaran');
    }
 
 
}
 