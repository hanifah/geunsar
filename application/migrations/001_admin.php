<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 

class Migration_admin extends CI_Migration
{
    
 
    public function up()
    {
    	if (!$this->db->table_exists('admin')) {
    		$this->dbforge->add_field( array(
    			'kd_admin' => array(
					'type'           => 'varchar',
					'constraint'     => 16,
					'unsigned'       => true
    			),
    			'nama_admin' => array(
					'type'       => 'varchar',
					'constraint' => 30,
					'null'       => false
    			),
				'alamat' => array(
					'type'       => 'text',
					'constraint' => 50,
					'null'       => false
    			),
    			'notelp' => array(
					'type'       => 'varchar',
					'constraint' => 13,
					'null'       => false
    			),
    			'email' => array(
					'type'       => 'varchar',
					'constraint' => 30,
					'null'       => false
    			),
    		));
 
    		
    		$this->dbforge->add_key('kd_admin', true);
    		$this->dbforge->create_table('admin');
 
    	} 
    }
 
   
    
    public function down()
    {
    	$this->dbforge->drop_table('admin');
    }
 
 
}
 