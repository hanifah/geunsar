<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 

class Migration_transaksi extends CI_Migration
{
    
 
    public function up()
    {
    	if (!$this->db->table_exists('transaksi')) {
    		$this->dbforge->add_field( array(
    			'no_ktp' => array(
					'type'           => 'varchar',
					'constraint'     => 16,
					'unsigned'       => true
    			),
    			'nama_pelanggan' => array(
					'type'       => 'varchar',
					'constraint' => 30,
					'null'       => false
    			),
				'notelp' => array(
					'type'       => 'varchar',
					'constraint' => 13,
					'null'       => true
    			),
    			'email' => array(
					'type'       => 'varchar',
					'constraint' => 30,
					'null'       => false
    			),
				'kd_kamar' => array(
					'type'       => 'varchar',
					'constraint' => 3,
					'null'       => false
    			),
				'status' => array(
					'type'       => 'varchar',
					'constraint' => 10,
					'null'       => false
    			),
    			
    		));
 
    		
    		$this->dbforge->add_key('no_ktp', true);
    		$this->dbforge->create_table('transaksi');
 
    	} 
    }
 
   
    
    public function down()
    {
    	$this->dbforge->drop_table('transaksi');
    }
 
 
}
 