<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 

class Migration_kamar extends CI_Migration
{
    
 
    public function up()
    {
    	if (!$this->db->table_exists('kamar')) {
    		$this->dbforge->add_field( array(
    			'kd_kamar' => array(
					'type'           => 'varchar',
					'constraint'     => 3,
					'unsigned'       => true
    			),
    			'jenis_kamar' => array(
					'type'       => 'varchar',
					'constraint' => 10,
					'null'       => false
    			),
				'harga_kamar' => array(
					'type'       => 'float',
					'constraint' => 15,
					'null'       => false
    			),
    		));
 
    		
    		$this->dbforge->add_key('kd_kamar', true);
    		$this->dbforge->create_table('kamar');
 
    	} 
    }
 
   
    
    public function down()
    {
    	$this->dbforge->drop_table('kamar');
    }
 
 
}
 